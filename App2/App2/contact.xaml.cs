﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class contact : ContentPage
    {
        Page1 home;
        public contact(Page1 Pageprofil, string choice)
        {
            InitializeComponent();
            choice_contact.Text = choice;
            home = Pageprofil;
            int view_contact = Page1.add_view(Pageprofil);
            contact_visits.Text = view_contact.ToString();
        }


        protected override void OnDisappearing()
        {
            Page1.add_view(home);
        }
    }
}