﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2
{
	public partial class myprofile : ContentPage
	{
        Page1 home;
		public myprofile (Page1 Pageprofil)
		{
			InitializeComponent ();
            home = Pageprofil;
            int view_pro = Page1.add_view(Pageprofil);
            profil_visits.Text = view_pro.ToString();


        }

        public async void RaiseBackButtonPressed2()
        {
            await DisplayAlert("Good bye !", "You come back on the Home page. See you soon.", "OK good bye !");
        }

        
        protected override void OnDisappearing()
        {
            RaiseBackButtonPressed2();
            Page1.add_view(home);
        }



    }
}