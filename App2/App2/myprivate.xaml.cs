﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2
{
    public partial class myprivate : ContentPage
    {
        Page1 home;
        public myprivate(Page1 Pageprofil)
        {
            InitializeComponent();
            home = Pageprofil;
            int view_private = Page1.add_view(Pageprofil);
            private_visits.Text = view_private.ToString();
        }


        protected override void OnDisappearing()
        {
            Page1.add_view(home);
        }
    }
}