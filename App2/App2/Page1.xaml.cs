﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2
{
    
    public partial class Page1 : ContentPage
    {
        int views = 1;
        public Page1()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        void my_profile_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new myprofile(this));
        }

        async void my_private_Tapped(object sender, System.EventArgs e)
        {
            bool alertResponse = await DisplayAlert("It is private !", "Do you want continue ?", "Yes i am curious!", "No... it is private");

            if (alertResponse == true)
            {
                await Navigation.PushAsync(new myprivate(this));
            }
        }

        async void my_contact_Tapped(object sender, System.EventArgs e)
        {

            string choice = await DisplayActionSheet("Overloaded Action Sheet",
                              "Cancel, bad idea.",
                              null,
                              "By email",
                              "By fax",
                              "By bird",
                              "By bye");

                await Navigation.PushAsync(new contact(this, choice));
        }

        public static int add_view(Page1 Hp)
        {
            Hp.views = Hp.views + 1;
            Hp.visits.Text = Hp.views.ToString();
            return Hp.views;
        }
    }
}